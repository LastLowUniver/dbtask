import { Component, OnInit } from '@angular/core';
import {lastValueFrom} from "rxjs";
import {ValueService} from "../services/value.service";

@Component({
  selector: 'app-analyt-one',
  templateUrl: './analyt-one.component.html',
  styleUrls: ['./analyt-one.component.sass']
})
export class AnalytOneComponent implements OnInit {

  constructor(private valueService: ValueService) { }

  ngOnInit(): void {
    this.getData().then(r => {})
  }

  elements: any = []
  isLoad: boolean = false

  async getData() {
    this.isLoad = true
    this.elements = await lastValueFrom(this.valueService.getAnalytOne()).then(r => {this.isLoad = false; return r})
  }

}
