import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { NavbarComponent } from './navbar/navbar.component';
import {MainComponent} from "./main/main.component";
import {ValueService} from "./services/value.service";
import { ButtonComponent } from './active_com/button/button.component';
import { RefreshComponent } from './active_com/refresh/refresh.component';
import { ModalWindowComponent } from './active_com/modal-window/modal-window.component';
import {FormsModule} from "@angular/forms";
import { TableOneComponent } from './table-one/table-one.component';
import { TableTwoComponent } from './table-two/table-two.component';
import { ShowModelOneComponent } from './table-one/show-model-one/show-model-one.component';
import { ShowModelTwoComponent } from './table-two/show-model-two/show-model-two.component';
import { TableComponent } from './active_com/table/table.component';
import { AnalytOneComponent } from './analyt-one/analyt-one.component';
import { AnalytTwoComponent } from './analyt-two/analyt-two.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MainComponent,
    ButtonComponent,
    RefreshComponent,
    ModalWindowComponent,
    TableOneComponent,
    TableTwoComponent,
    ShowModelOneComponent,
    ShowModelTwoComponent,
    TableComponent,
    AnalytOneComponent,
    AnalytTwoComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
  providers: [
    ValueService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
