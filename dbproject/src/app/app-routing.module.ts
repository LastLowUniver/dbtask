import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CommonModule} from "@angular/common";
import {MainComponent} from "./main/main.component";
import {TableOneComponent} from "./table-one/table-one.component";
import {TableTwoComponent} from "./table-two/table-two.component";
import {AnalytOneComponent} from "./analyt-one/analyt-one.component";
import {AnalytTwoComponent} from "./analyt-two/analyt-two.component";

export const routes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: 'db/1',
    component: TableOneComponent
  },
  {
    path: 'db/2',
    component: TableTwoComponent
  },
  {
    path: 'an/1',
    component: AnalytOneComponent
  },
  {
    path: 'an/2',
    component: AnalytTwoComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
