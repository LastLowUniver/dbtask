import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ValueService} from "../../services/value.service";
import {lastValueFrom} from "rxjs";

@Component({
  selector: 'app-show-model-one',
  templateUrl: './show-model-one.component.html',
  styleUrls: ['./show-model-one.component.sass']
})
export class ShowModelOneComponent implements OnInit {

  constructor(private valueService: ValueService) { }

  @Input() set item (val: any) {
    this._item = {...val}
  }
  @Input() label: string = "Not label"

  @Output() closeWindow = new EventEmitter();
  @Output() clickChange = new EventEmitter();

  _item: any = []

  async ngOnInit() {

  }

}
