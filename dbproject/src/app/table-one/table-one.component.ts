import { Component, OnInit } from '@angular/core';
import {lastValueFrom} from "rxjs";
import {ValueService} from "../services/value.service";

@Component({
  selector: 'app-table-one',
  templateUrl: './table-one.component.html',
  styleUrls: ['./table-one.component.sass']
})
export class TableOneComponent implements OnInit {

  constructor(private valueService: ValueService) { }

  elements: any = []
  isLoad: boolean = false
  isShowModel: boolean = false
  modelLabel: string = "Добавить значение"
  changeItem: any = []
  ActionName: boolean = false

  ngOnInit(): void {
    this.getData().then(r => {return r});
  }

  async getData() {
    this.isLoad = true
    this.elements = await lastValueFrom(this.valueService.getUsers()).then((res: any) => {this.isLoad = false; return res})
  }

  change_elem(name_params: string, item: any = null) {
    if (name_params == "add") {
      this.changeItem = []
      this.modelLabel = "Добавить значение"
      this.ActionName = false
    }else {
      this.changeItem = item
      this.modelLabel = "Изменить значения"
      this.ActionName = true
    }
    this.isShowModel = true
  }

  async clickChange(changeItem: any) {
    if (this.ActionName) {
      await this.valueService.update("users", changeItem).subscribe(res => {
        if (res == null) {
          console.log("Что-то не то")
        }else {
          this.getData().then(r => {return r});
        }
      });
    }else {
      await this.valueService.insert("users", changeItem).subscribe(res => {
        if (res == null) {
          console.log("Что-то не то")
        }else {
          this.getData().then(r => {return r});
        }
      });
    }
    this.isShowModel = false
  }

  closeWindow() {
    this.isShowModel = false
  }

  deleteElem(item: any) {
    this.valueService.delete("users", item.id).subscribe(res => {
      if (res == null) {
        console.log("Что-то не то")
      }else {
        this.getData().then(r => {return r});
      }
    });
  }
}
