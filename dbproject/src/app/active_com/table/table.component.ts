import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styles: [
  ]
})
export class TableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() elements: any = [];
  @Input() name: string = "Not name"
  @Input() isShowAdd: boolean = true
  @Input() isShowBetween: boolean = false
  @Output() clickAdd = new EventEmitter();
  @Output() loadBetween = new EventEmitter();

  dateFrom: string = "2010-01-01";
  dateTo: string = "2022-10-10";
}
