import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-button',
  template: `
    <button>
      <span *ngIf="!isIcon" class="text">{{label}}</span>
      <span *ngIf="isIcon" class="material-icons">{{label}}</span>
    </button>
  `,
  styles: [
    `
@import "../../../variables"
span
  padding: 5px
  color: $main-color
button
  border: none
  background: $main-second-color
  border-radius: 5px
  cursor: pointer
  transition: all .1s linear
  &:hover
    box-shadow:  1px 1px 12px #848484
.text
  display: inline-block
  padding: 5px 10px
  font-size: 1.1em
  font-weight: 500
    `
  ]
})
export class ButtonComponent implements OnInit {

  constructor() { }

  @Input() label: string = 'Not label'
  @Input() isIcon: boolean = false


  ngOnInit(): void {
  }

}
