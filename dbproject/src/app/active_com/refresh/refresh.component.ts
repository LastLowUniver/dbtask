import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-refresh',
  template: `
    <div class="spin-wrapper">
      <div class="spinner">
      </div>
    </div>
  `,
  styles: [
`
  @import "../../../variables"
  .spin-wrapper
    width: 100%
    z-index: 1000
    height: 100vh
    background: rgba(31, 31, 31, 0.38)
    position: absolute
    transform: translate(-50%, -50%)
    top: 50%
    left: 50%

    .spinner
      position: absolute
      height: 60px
      width: 60px
      border: 3px solid transparent
      border-top-color: $main-second-color
      top: 50%
      left: 50%
      margin: -30px
      border-radius: 50%
      animation: spin 1s linear infinite

      &:before, &:after
        content: ''
        position: absolute
        border: 3px solid transparent
        border-radius: 50%


      &:before
        border-top-color: $header
        top: -12px
        left: -12px
        right: -12px
        bottom: -12px
        animation: spin 2s linear infinite

      &:after
        border-top-color: #FFFBFE
        top: 6px
        left: 6px
        right: 6px
        bottom: 6px
        animation: spin 3s linear infinite

  @keyframes spin
    0%
      transform: rotate(0deg)
    100%
      transform: rotate(360deg)
`
  ]
})
export class RefreshComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
