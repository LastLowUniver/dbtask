import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ValueService} from "../../services/value.service";
import {lastValueFrom} from "rxjs";

@Component({
  selector: 'app-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.sass']
})
export class ModalWindowComponent implements OnInit {

  constructor(private valueService: ValueService) { }

  @Output() closeWindow = new EventEmitter();
  @Output() clickChange = new EventEmitter();

  @Input() label: string = "Change element"

  ngOnInit() {
  }

}
