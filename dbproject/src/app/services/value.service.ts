import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ValueService{

  constructor(private httpClient: HttpClient) {}

  getAll(nameTable: string) {
    return this.httpClient.get(`${environment.api}/getall?table=${nameTable}`);
  }

  getOne(table: string, id: string) {
    return this.httpClient.get(`${environment.api}/getone?table=${table}&id=${id}`);
  }

  getUsers() {
    return this.httpClient.get(`${environment.api}/users`)
  }

  getCategory() {
    return this.httpClient.get(`${environment.api}/category`)
  }

  getAnalytOne() {
    return this.httpClient.get(`${environment.api}/getanalytone`)
  }

  getAnalytTwo(dateFrom: string, dateTo: string) {
    return this.httpClient.get(`${environment.api}/getanalyttwo?datefrom=${dateFrom}&dateto=${dateTo}`)
  }

  insert(table: string, params: any) {
    return this.httpClient.post(`${environment.api}/insert?table=${table}`, params);
  }

  update(table: string, params: any) {
    return this.httpClient.post(`${environment.api}/update?table=${table}`, params);
  }

  delete(table: string, id: any) {
    return this.httpClient.get(`${environment.api}/delete?table=${table}&id=${id}`);
  }
}
