import { Component, OnInit } from '@angular/core';
import {lastValueFrom} from "rxjs";
import {ValueService} from "../services/value.service";

@Component({
  selector: 'app-analyt-two',
  templateUrl: './analyt-two.component.html',
  styles: [
  ]
})
export class AnalytTwoComponent implements OnInit {

  constructor(private valueService: ValueService) { }

  ngOnInit(): void {
    this.getData().then(r => {})
  }

  elements: any = []
  isLoad: boolean = false
  dateFrom: string = '2010-01-01'
  dateTo: string = '2022-10-10'

  async getData() {
    this.isLoad = true
    this.elements = await lastValueFrom(this.valueService.getAnalytTwo(this.dateFrom, this.dateTo)).then(r => {this.isLoad = false; return r})
  }

  loadBetween(item: any) {
    this.dateFrom = item.dateFrom
    this.dateTo = item.dateTo
    this.getData().then(r => {console.log(this.dateFrom, this.dateTo)})

  }

}
