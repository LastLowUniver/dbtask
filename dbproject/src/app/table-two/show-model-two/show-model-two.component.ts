import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ValueService} from "../../services/value.service";
import {lastValueFrom} from "rxjs";


@Component({
  selector: 'app-show-model-two',
  templateUrl: './show-model-two.component.html',
  styleUrls: ['./show-model-two.component.sass']
})
export class ShowModelTwoComponent implements OnInit {

  constructor(public valueService: ValueService) { }

  @Input() set item (val: any) {
    this._item = {...val}
    if (val.user_id) {
      this.fillDefault().then(r => {return r})
    }
  }
  @Input() label: string = "Not label"

  @Output() closeWindow = new EventEmitter();
  @Output() clickChange = new EventEmitter();

  _item: any = []
  options: any = []
  users: any = []
  defaultOption: any = []
  defaultUser: any = []

  async ngOnInit() {
    this.options = await lastValueFrom(this.valueService.getAll('options')).then(res => {return res})
    this.users = await lastValueFrom(this.valueService.getAll('users')).then(res => {return res})
  }

  async fillDefault() {
    this.defaultOption = await lastValueFrom(this.valueService.getOne('options', this._item.options_id)).then(r => {return r})
    this.defaultUser = await lastValueFrom(this.valueService.getOne('users', this._item.user_id)).then(r => {return r})
  }

}
