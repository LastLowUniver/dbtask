import { Component, OnInit } from '@angular/core';
import {ValueService} from "../services/value.service";
import {lastValueFrom} from "rxjs";

@Component({
  selector: 'app-table-two',
  templateUrl: './table-two.component.html',
  styleUrls: ['./table-two.component.sass']
})
export class TableTwoComponent implements OnInit {

  constructor(private valueService: ValueService) { }


  elements: any = []
  isLoad: boolean = false
  isShowModel: boolean = false
  modelLabel: string = "Добавить значение"
  changeItem: any = []
  ActionName: boolean = false

  ngOnInit(): void {
    this.getData().then(r => {return r});
  }

  async getData() {
    this.isLoad = true
    this.elements = await lastValueFrom(this.valueService.getCategory()).then((res: any) => {this.isLoad = false; return res})
  }

  change_elem(name_params: string, item: any = null) {
    if (name_params == "add") {
      this.changeItem = []
      this.modelLabel = "Добавить значение"
      this.ActionName = false
    }else {
      this.changeItem = item
      this.modelLabel = "Изменить значения"
      this.ActionName = true
    }
    this.isShowModel = true
  }

  async clickChange(changeItem: any) {
    if (this.ActionName) {
      let {options_name, user_name, ...result_update} = changeItem;

      await this.valueService.update("category", result_update).subscribe(res => {
        if (res == null) {
          console.log("Что-то не то")
        }else {
          this.getData().then(r => {return r});
        }
      });
    }else {
      await this.valueService.insert("category", changeItem).subscribe(res => {
        if (res == null) {
          console.log("Что-то не то")
        }else {
          this.getData().then(r => {return r});
        }
      });
    }
    this.isShowModel = false
  }

  closeWindow() {
    this.isShowModel = false
  }

  deleteElem(item: any) {
    this.valueService.delete("category", item.id).subscribe(res => {
      if (res == null) {
        console.log("Что-то не то")
      }else {
        this.getData().then(r => {return r});
      }
    });
  }

}
