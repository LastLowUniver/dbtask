<?php
namespace Models;

use Services\Db;

class PreparedQuery {
    
    public static function getAll(string $table): ?array
    {
        $db = new Db();
        return $db->query('SELECT * FROM `' . $table . '`;', []);
    }

    public static function getOne(string $table, string $id) {
        $db = new Db();
        return $db->query('SELECT * FROM ' . $table . ' WHERE id = :id', ['id' => $id]);
    }

    public static function getUsers() {
        $db = new Db();
        return $db->query('SELECT id, name, register_date FROM users', []);
    }

    public static function getCategory(): ?array {
        $db = new Db();
        return $db->query(
            'SELECT category.id as `id`,
            category.name as `name`,
            category.id_options as `id_options`,
            options.name as `options_name`,
            category.user_id as `user_id`,
            users.name as `user_name`
            FROM category
            JOIN options ON category.id_options = options.id
            JOIN users ON users.id = category.user_id
            ', 
            []
        );
    }

    public static function getAnalytQueryOne() {
        $db = new Db();
        return $db->query("SELECT COUNT(*) as count, category.name as name FROM words JOIN category ON category.id = words.category_id GROUP BY category.name");
    }

    public static function getAnalytQueryTwo(string $dateFrom, string $dateTo) {
        $db = new Db();
        $query = "SELECT * FROM syncing JOIN users ON users.id = syncing.id WHERE sync_date BETWEEN ' " . $dateFrom . " ' AND ' " . $dateTo . " ' ";
        return $db->query($query);
    }

    public static function insert(string $table, array $params) {
        $db = new Db();
        $qust = ''; 
        $i = 1;
        foreach($params as $key => $value) {
            $qust = $qust . ':' . $key;
            if (count($params) != $i) $qust = $qust . ', ';
            $i++;
        }
        return $db->query('INSERT INTO ' . $table . ' (' . implode(', ', array_keys($params)) . ') VALUES ('. $qust .')', $params);
    }
    
    public static function update(string $table, array $params) {
        $db = new Db();
        $qust = '';
        $i = 2;
        foreach($params as $key => $value) {
            if ($key != 'id') {
                $qust = $qust . $key. '=:' . $key;
                if (count($params) != $i) $qust = $qust . ', ';
                $i++;
            }
        }
        return $db->query('UPDATE '. $table . ' SET ' . $qust . ' WHERE id = :id', $params);
    }

    public static function delete(string $table, string $id) {
        $db = new Db();
        return $db->query('DELETE FROM '. $table . ' WHERE id = ?', [$id]);
    }
}