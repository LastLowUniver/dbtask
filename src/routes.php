<?php

return [
    '~^getall$~' => [\Controllers\MainController::class, 'getAll'],
    '~^getone$~' => [\Controllers\MainController::class, 'getOne'],
    '~^getall_in_array$~' => [\Controllers\MainController::class, 'getAllInArray'],
    '~^users$~' => [\Controllers\MainController::class, 'getUsers'],
    '~^category$~' => [\Controllers\MainController::class, 'getCategory'],
    '~^getanalytone$~' => [\Controllers\MainController::class, 'getAnalytQueryOne'],
    '~^getanalyttwo$~' => [\Controllers\MainController::class, 'getAnalytQueryTwo'],
    '~^insert$~' => [\Controllers\MainController::class, 'insert'],
    '~^update$~' => [\Controllers\MainController::class, 'update'],
    '~^delete$~' => [\Controllers\MainController::class, 'delete']
];