<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');

// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);

$_POST = json_decode(file_get_contents("php://input"),true);

spl_autoload_register(function (string $className) {
    require_once __DIR__ . '/../src/' . str_replace('\\', '/', $className) . '.php';
});

$routes = require __DIR__ . '/../src/routes.php';

$routeIsFound = false;

foreach ($routes as $pattern => $controller) {
    preg_match($pattern, $_GET['page'], $matches);
    if (!empty($matches)) {
        $routeIsFound = true;
        break;
    }
}

if (!$routeIsFound) {
    echo 'Not found';
    die();
}

$controllerName = $controller[0];
$actionName = $controller[1];

$controllerA = new $controllerName();
$controllerA->$actionName($_GET['method'] == 'GET' ? $_GET : $_POST, isset($_GET['table']) ? $_GET['table'] : null);