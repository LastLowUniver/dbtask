<?php

namespace Controllers;

use \Models\PreparedQuery;

class MainController {
    public static function getAll(array $elems) {
        echo json_encode(PreparedQuery::getAll($elems['table']));
    }

    public static function getOne(array $elems) {
        echo json_encode(PreparedQuery::getOne($elems['table'], $elems['id']));
    }

    public static function getAllInArray(array $elems) {
        $result = [];
        foreach($elems as $val) {
            $result[$val] = PreparedQuery::getAll($val);
        }
        echo json_encode($result);
    }

    public static function getUsers() {
        echo json_encode(PreparedQuery::getUsers());
    }

    public static function getCategory() {
        echo json_encode(PreparedQuery::getCategory());
    }

    public static function getAnalytQueryOne() {
        echo json_encode(PreparedQuery::getAnalytQueryOne());
    }

    public static function getAnalytQueryTwo(array $elems) {
        echo json_encode(PreparedQuery::getAnalytQueryTwo($elems['datefrom'], $elems['dateto']));
    }

    public static function insert(array $elems, string $table) {
        echo json_encode(PreparedQuery::insert($table, $elems));
    }

    public static function update(array $elems, string $table) {
        echo json_encode(PreparedQuery::update($table, $elems));
    }
    
    public static function delete(array $elems) {
        echo json_encode(PreparedQuery::delete($elems['table'], $elems['id']));
    }
}